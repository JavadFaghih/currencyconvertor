//
//  DownloadServices.swift
//  CurrencyConvertor
//
//  Created by javad faghih on 7/1/1399 AP.
//

import Foundation
import Alamofire

class RateService {
    
    static let instance = RateService()
    
    
    func downloadRate(completed: @escaping (Bool) -> ()) {
        
        let url = URL(string: API_URL)
        AF.request(url!).responseJSON { (response) in
            
           
            switch response.result {
          
            case .success(let value):
                //parse data here
            
                
                self.PrseFunction(json: value)
                
                completed(true)
            
            case .failure(let error):
                //handle error here
            
          
                
                completed(false)
            }
            
            
            
        }
        
        
    }
    
    
}
