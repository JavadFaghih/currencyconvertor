//
//  ExchangeModel.swift
//  CurrencyConvertor
//
//  Created by javad faghih on 7/1/1399 AP.
//

import Foundation

final class Currency {
   
    private(set) public var from: String
    private(set) public var to: String
    private(set) public var from_amount: Double
    private(set) public var to_amount: Double
   
    
    init(from: String, to: String, from_amount: Double, to_amount: Double) {
        self.from = from
        self.to = to
        self.from_amount = from_amount
        self.to_amount = to_amount
       
    }
    
}
